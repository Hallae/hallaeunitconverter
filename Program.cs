﻿using System;
using System.Resources;
using System.Globalization;
using System.Xml.Linq;


namespace HallaeUnitConverter

{
    internal class HallaeUnitConverter
    {
        static void Main(string[] args)
        {
            


            while (true)
            {
                Console.WriteLine("Welcome to The HallaeUnitconverter! :)");
                Console.WriteLine("What would you like to convert?");
                Console.WriteLine("Please select from one of the following options");
                Console.WriteLine("1. Miles to Kilometers");
                Console.WriteLine("2. Pounds to Kilograms");
                Console.WriteLine("3. Fahrenheit to Celsius");
                Console.WriteLine("4. Foot to Inches");
                Console.WriteLine("5. Yard to Feet");
                Console.WriteLine("6. Miles to Feet");
                Console.WriteLine("7. Miles to yard"); 
                Console.WriteLine("8. Pounds to Ounces");
                Console.WriteLine("0. Exit");
                String input = Console.ReadLine();

                // Validate user input
                if (input == "1")
                {
                    MilesToKilometers();
                }
                else if (input == "2")
                {
                    PoundsToKilograms();
                }
                else if (input == "3")
                {
                    FahrenheitToCelsius();
                }

                else if (input == "4")
                {
                    FootToInches();
                }

                else if (input == "5")
                {
                    YardToFeet();
                }

                else if (input == "6")
                {
                    MilesToFeet();
                }

                else if (input == "7") 
                {
                    MilesToYard();
                }

                  else if (input == "8") 
                {
                    PoundsToOunces();
                }

                else if (input == "0")
                {
                    Console.WriteLine("Thank you, have a nice day " + "!");
                    Console.WriteLine("Please come convert with us again soon!");

                    Environment.Exit(1);

                }


                else
                {
                    Console.WriteLine("Invalid input. Please try again.");
                }
            }
        }

        static void MilesToKilometers()
        {
            Console.WriteLine("Please enter the number of miles: ");
            double miles = double.Parse(Console.ReadLine());
            double kilometers = miles * 1.609344;
            Console.WriteLine("{0} miles is equal to {1} kilometers.", miles, kilometers);
        }

        static void PoundsToKilograms()
        {
            Console.WriteLine("Please enter the number of pounds: ");
            double pounds = double.Parse(Console.ReadLine());
            double kilograms = pounds * 0.45359237;
            Console.WriteLine("{0} pounds is equal to {1} kilograms.", pounds, kilograms);
        }

        static void FahrenheitToCelsius()
        {
            Console.WriteLine("Please enter the temperature in Fahrenheit: ");
            double fTemp = double.Parse(Console.ReadLine());
            double cTemp = (fTemp - 32) * 5 / 9;
            Console.WriteLine("{0} degrees Fahrenheit is equal to {1} degrees Celsius.", fTemp, cTemp);
        }
        static void FootToInches()
        {
            Console.WriteLine("Please enter the number of foot: ");
            double foot = double.Parse(Console.ReadLine());
            double inches = foot * 12;
            Console.WriteLine("{0} foot is equal to {1} Inches.", foot, inches);
        }

        static void YardToFeet()
        {
            Console.WriteLine("Please enter the number of yard: ");
            double yard = double.Parse(Console.ReadLine());
            double feet = yard*3;
            Console.WriteLine("{0} yard is equal to {1} feet.", yard, feet); 
        }

        static void MilesToFeet()
        {
            Console.WriteLine("Please enter the number of Miles: ");
            double miles = double.Parse(Console.ReadLine());
            double feet = miles * 5280;
            Console.WriteLine("{0} miles is equal to {1} feet.", miles, feet); 
        }



        static void MilesToYard()
        {
            Console.WriteLine("Please enter the number of Miles: ");
            double miles = double.Parse(Console.ReadLine());
            double yard = miles * 1760;
            Console.WriteLine("{0} miles is equal to {1} yard.", miles, yard);
        }

        static void PoundsToOunces()
        {
            Console.WriteLine("Please enter the number of Pounds: ");
            double Pounds = double.Parse(Console.ReadLine());
            double Ounces = Pounds * 16;
            Console.WriteLine("{0} Pounds is equal to {1} Ounces.", Pounds, Ounces);
        }







    }

    
}